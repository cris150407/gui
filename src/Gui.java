import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.*;
import java.awt.*;

public class Gui {

	public static void main(String args[]) {
		JFrame frame = new JFrame("Mi primer Gui");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(500, 500);

		JMenuBar mb = new JMenuBar();
		JMenu m1 = new JMenu("Archivo");
		JMenu m2 = new JMenu("Ayuda");
		mb.add(m1);
		mb.add(m2);
		JMenuItem m11 = new JMenuItem("Abrir");
		JMenuItem m22 = new JMenuItem("Guardar como");
		m1.add(m11);
		m1.add(m22);

		JPanel panel = new JPanel();// Creando el panel en la parte inferior y agregando componentes
		JLabel Label = new JLabel("Introducir texto");
		JTextField tf = new JTextField(10);// acepta hasta 10 caracteres
		JButton send = new JButton("Enviar");
		JButton reset = new JButton("Restablecer");
		panel.add(Label);// Componentes agregados ussando Flow Layout
		panel.add(Label);// Componentes agregados usando Flow layout
		panel.add(tf);
		panel.add(send);
		panel.add(reset);

		JTextArea ta = new JTextArea();

		JButton boton = new JButton("Dale Clic");
		JButton button2 = new JButton("Listo");
		frame.getContentPane().add(boton);// Agrega el bot�n al panel de contenido
		frame.getContentPane().add(BorderLayout.SOUTH, panel);
		frame.getContentPane().add(BorderLayout.NORTH, mb);
		frame.getContentPane().add(BorderLayout.CENTER, ta);
		frame.setVisible(true);

	}

}
